﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace BookLibrary
{
    class MainTesting
    {
        IWebDriver driver;
        [SetUp]
        public void Setup()
        {
            driver = new FirefoxDriver();
            driver.Navigate().GoToUrl("http://localhost/index.html");
            driver.Manage().Window.Maximize();
        }

        [Test(Description = "Search for book title")]
        public void SearchBookTitle()
        {
            Assert.AreEqual("Library Page", driver.Title, "La página no fue encontrada", null);
            IWebElement searchBar = driver.FindElement(By.XPath("//div[5]/table/*[@id='search_bar']"));
            this.TypeOnWebElement(searchBar, "Learning Selenium WebDriver in C#");
            IWebElement searchResult = driver.FindElement(By.XPath("//div[3]/table/*[@class='book_title']"));
            Assert.AreEqual("Learning Selenium WebDriver in C#", searchResult.Text, "El libro buscado no fue encontrado", null);
        }
        [Test(Description = "Search for book author")]
        public void SearchBookAuthor()
        {
            Assert.AreEqual("Library Page", driver.Title, "La página no fue encontrada", null);
            IWebElement searchBar = driver.FindElement(By.XPath("//div[5]/table/*[@id='search_bar']"));
            this.TypeOnWebElement(searchBar, "Author: Stephen Hawking");
            IWebElement searchResult = driver.FindElement(By.XPath("//div[3]/table/*[@class='book_author']"));
            Assert.AreEqual("Author: Stephen Hawking", searchResult.Text, "El autor buscado no fue encontrado", null);
        }

        [TearDown]
        public void Teardown()
        {
            driver.Close();
        }

        private void TypeOnWebElement(IWebElement element, string textToType)
        {
            element.Clear();
            element.SendKeys(textToType);
        }
    }
}
